package com.ranull.craftableconcrete;

import com.ranull.craftableconcrete.recipe.RecipeManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class CraftableConcrete extends JavaPlugin {
    private RecipeManager recipeManager;

    @Override
    public void onEnable() {
        recipeManager = new RecipeManager(this);

        recipeManager.loadRecipes();
    }

    @Override
    public void onDisable() {
        recipeManager.removeRecipes();
    }
}
