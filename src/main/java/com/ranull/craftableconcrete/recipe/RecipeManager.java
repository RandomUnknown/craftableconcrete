package com.ranull.craftableconcrete.recipe;

import com.ranull.craftableconcrete.CraftableConcrete;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;

import java.util.Iterator;

public class RecipeManager {
    private CraftableConcrete plugin;

    public RecipeManager(CraftableConcrete plugin) {
        this.plugin = plugin;
    }

    public void loadRecipes() {
        createConcreteRecipe("white_concrete_recipe", Material.WHITE_DYE, Material.WHITE_CONCRETE);
        createConcreteRecipe("orange_concrete_recipe", Material.ORANGE_DYE, Material.ORANGE_CONCRETE);
        createConcreteRecipe("magenta_concrete_recipe", Material.MAGENTA_DYE, Material.MAGENTA_CONCRETE);
        createConcreteRecipe("light_blue_concrete_recipe", Material.LIGHT_BLUE_DYE, Material.LIGHT_BLUE_CONCRETE);
        createConcreteRecipe("yellow_concrete_recipe", Material.YELLOW_DYE, Material.YELLOW_CONCRETE);
        createConcreteRecipe("lime_concrete_recipe", Material.LIME_DYE, Material.LIME_CONCRETE);
        createConcreteRecipe("pink_concrete_recipe", Material.PINK_DYE, Material.PINK_CONCRETE);
        createConcreteRecipe("gray_concrete_recipe", Material.GRAY_DYE, Material.GRAY_CONCRETE);
        createConcreteRecipe("light_gray_concrete_recipe", Material.LIGHT_GRAY_DYE, Material.LIGHT_GRAY_CONCRETE);
        createConcreteRecipe("cyan_concrete_recipe", Material.CYAN_DYE, Material.CYAN_CONCRETE);
        createConcreteRecipe("purple_concrete_recipe", Material.PURPLE_DYE, Material.PURPLE_CONCRETE);
        createConcreteRecipe("blue_concrete_recipe", Material.BLUE_DYE, Material.BLUE_CONCRETE);
        createConcreteRecipe("brown_concrete_recipe", Material.BROWN_DYE, Material.BROWN_CONCRETE);
        createConcreteRecipe("green_concrete_recipe", Material.GREEN_DYE, Material.GREEN_CONCRETE);
        createConcreteRecipe("red_concrete_recipe", Material.RED_DYE, Material.RED_CONCRETE);
        createConcreteRecipe("black_concrete_recipe", Material.BLACK_DYE, Material.BLACK_CONCRETE);
    }

    public void removeRecipes() {
        Iterator<Recipe> recipes = plugin.getServer().recipeIterator();
        Recipe recipe;
        while (recipes.hasNext()) {
            recipe = recipes.next();
            if (recipe == null) {
                continue;
            }
            Material type = recipe.getResult().getType();
            if (type.equals(Material.WHITE_CONCRETE) ||
                    type.equals(Material.ORANGE_CONCRETE) ||
                    type.equals(Material.MAGENTA_CONCRETE) ||
                    type.equals(Material.LIGHT_BLUE_CONCRETE) ||
                    type.equals(Material.YELLOW_CONCRETE) ||
                    type.equals(Material.LIME_CONCRETE) ||
                    type.equals(Material.PINK_CONCRETE) ||
                    type.equals(Material.GRAY_CONCRETE) ||
                    type.equals(Material.LIGHT_GRAY_CONCRETE) ||
                    type.equals(Material.CYAN_CONCRETE) ||
                    type.equals(Material.PURPLE_CONCRETE) ||
                    type.equals(Material.BLUE_CONCRETE) ||
                    type.equals(Material.BROWN_CONCRETE) ||
                    type.equals(Material.GREEN_CONCRETE) ||
                    type.equals(Material.RED_CONCRETE) ||
                    type.equals(Material.BLACK_CONCRETE)) {
                recipes.remove();
            }
        }
    }

    public void createConcreteRecipe(String keyName, Material dye, Material result) {
        ItemStack itemStack = new ItemStack(result, 8);
        NamespacedKey namespacedKey = new NamespacedKey(plugin, keyName);
        ShapedRecipe shapedRecipe = new ShapedRecipe(namespacedKey, itemStack);

        shapedRecipe.shape("DSS", "SSC", "CCC");
        shapedRecipe.setIngredient('D', dye);
        shapedRecipe.setIngredient('S', Material.SAND);
        shapedRecipe.setIngredient('C', Material.COBBLESTONE);
        shapedRecipe.setGroup("concrete");
        plugin.getServer().addRecipe(shapedRecipe);
    }
}
